<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <meta name="theme-color" content="#2196F3">
    <meta charset="utf-8">
    <meta name="description" content="consultoria Fiscal y Contable. Colabora de manera efectiva, segura y productiva, integra departamentos, sistemas contpaq i.">

    <meta name="keywords" content="mtzdelatorre, Fiscal, capacitacion, contable, sistemas contpaq i, recursos humanos, despacho contable, empresas,Martinez de la torre.">

    <meta name="robots" content="index,follow">
    <meta name="distribution" content="global">
    <meta http-equiv="pragma" content="no-cache">
    <meta property="og:title" content="Martinez de la torre.">
    <meta property="og:type" content="company">
    <meta property="og:url" content="https://www.mtzdelatorre.com">
    <meta property="og:image" content="img/logo2.jpg">
    <meta property="og:site_name" content="Martinez de la torre">
    <meta property="og:description" content="mtzdelatorre, Fiscal, capacitacion, contable, sistemas contpaq i, recursos humanos, despacho contable, empresas,Martinez de la torre.">

    <!-- etiqueta proporcionada por arturo torre -->
    <!--meta name="google-site-verification" content="t1xkZ1OqaX6CTRxuGy5SG6liUc4ptxbF6G1s5pI7ON4" /-->
    <!-- /////////////fin de la etiqueta de arturo ////////////////////////////-->
    <link rel="icon" type="image/jpg" href="img/logo2.jpg" />
    <title>Martínez de la Torre</title>
    <!-- CSS  -->
    <link href="min/plugin-min.css" type="text/css" rel="stylesheet">
    <link href="css/font-awesome.min.css" type="text/css" rel="stylesheet" >
    <link href="min/custom-min.css" type="text/css" rel="stylesheet" >
</head>
<body id="top" class="scrollspy">

<!-- Pre Loader -->
<div id="loader-wrapper">
    <div id="loader"></div>

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>

<!--Navigation-->
 <div class="navbar-fixed">
    <nav id="nav_f" class="default_color" role="navigation">
        <div class="container">

            <div class="nav-wrapper">

            <img src="img/logo2.jpg" alt="" class="logo-baner">
             <div class="center header cd-headline" style="display: inline-block;line-height: 15px;">
                <span>Martínez de la Torre</span><br>
                <span style="font-size: 0.8em;">Contadores Público</span><br>
                <span class="cd-words-wrapper waiting">
                    <b class="is-visible">Fiscal,</b>
                    <b>Contable,</b>
                    <b>Sistemas CONTPAQ i</b>
                </span>
            </div>
                <ul class="right hide-on-med-and-down">
                    <li><a href="#intro">Servicios</a></li>
                    <li><a href="#work">Eventos</a></li>
                    <!--li><a href="#team">Equipo</a></li-->
                    <li><a href="#contact">Contacto</a></li>
                </ul>
                <ul id="nav-mobile" class="side-nav">
                    <li><a href="#intro">Servicios</a></li>
                    <li><a href="#work">Eventos</a></li>
                    <!--li><a href="#team">Equipo</a></li-->
                    <li><a href="#contact">Contacto</a></li>
                </ul>

            <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
            </div>
        </div>
    </nav>
</div>

<!--header-->
<div class="section no-pad-bot" id="index-banner">
    <div class="container">
        <div class="row">

            <div class="col s8">
                <div  class="col s12">
                <h2 class="center header " style="font-size: 1.26rem;line-height:2rem;color: #fff;">Somos una firma de contadores públicos en la cual brindamos a nuestros clientes los mejores servicios, para minimizar la carga tributaria dentro del <span class="span_h2">  marco legal existente.  </span>Somos la firma mas completa, por que contamos con las areas capacitadas para que nuestros clientes cumplan en tiempo y forma todas las disposiciones fiscales contando con el respaldo de un proveedor autorizado de certificación en materia de comprobantes fiscales, único en el mercado y respaldo de marca por mas <span class="span_h2"> de 27 años.</span> </h2>
            </div>

            <div  class="col s12 m4 l4">
                <div class="center promo promo-example">
                    <i class="mdi-image-flash-on"></i>
                    <h5 class="promo-caption">Mejor atencion</h5>
                    <!--p class="light center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Cum sociis natoque penatibus et magnis dis parturient montes.</p-->
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="center promo promo-example">
                    <i class="mdi-social-group"></i>
                    <h5 class="promo-caption">Experiencia Enfocada al Usuario</h5>
                    <!--p class="light center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p-->
                </div>
            </div>
            </div>
            <div class="col s4 anuncio-rigth-header">
            <h3 class="header">Novedades</h3>
                <ul class="collection">
                    <li>
                      <a href="eventos.html" ><img src="img/CURSO_8_ABRIL_2016.jpg" alt="" class="evento-img-baner-derecho"/></a>
                    </li>
                    <li class="collection-item avatar collection-item-set">
                      <img src="img/contaelectro.png" alt="" class="circle">
                      <span class="title" style="color: #26a69a;">Curso Taller</span>
                      <p style="font-size: 12px;">Análisis practico <br>
                          de contabilidad Electrónica
                      </p>
                      <a href="#work" onclick="showEvent(1);" class="secondary-content"><i class="fa fa-angle-double-right"></i></a>
                    </li>
                  </ul>
            </div>
        </div>
    </div>
</div>

<!--Intro y servicios-->
<!--div id="intro" class="section scrollspy">
    <div class="container">
        <div class="row">


        </div>
    </div>
</div-->

<!--Eventos-->
<div class="section scrollspy" id="work">
    <div class="container">
        <h2 class="header text_b">Eventos y Cursos </h2>
        <div class="row">
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image waves-effect waves-block waves-light">
                        <img class="activator" src="img/contaelectro.png">
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4">Análisis practico de contabilidad Electrónica<i class="mdi-navigation-more-vert right"></i></span>
                        <p class="text_f-evento">29 de febrero 2016</p>
                        <p><a onclick="showEvent(1);">Documentacion</a></p>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text">Análisis practico de contabilidad Electrónica <i class="mdi-navigation-close right"></i></span>
                        <p style="text-align: justify;">Analizar de manera practica las normativas implicadas en la contabilidad electronica donde el practicante  tendra que analizar el manejo del sistema contable, hacer uso de las herramientas para mejora administrativa, la eficiente contabilidad y se facilite el cumplimiento de las obligaciones establecidas por las leyes.</p>
                    </div>
                </div>
            </div>
            <div id="showEventosDiv">

            </div>
        </div>
        <div class="row">
            <h2 class="header text_b">Galería</h2>
            <div class="col s4">
                    <div class="card medium">
                        <div class="card-image">
                                <img src="files/eventos/galerya/1.jpeg" alt="">
                        </div>
                        <div class="card-content">
                          <span class="card-title activator grey-text text-darken-4">Evento Curso Taller.</span>
                          <p><a>Curso 29-feb-2016</a></p>
                        </div>
                    </div>
            </div>
            <div class="col s12 m4 l4">
                    <div class="card medium">
                        <div class="card-image">
                                <img src="files/eventos/galerya/2.jpeg" alt="">
                        </div>
                        <div class="card-content">
                          <span class="card-title activator grey-text text-darken-4">Evento Curso Taller.</span>
                          <p><a>Curso 29-feb-2016</a></p>
                        </div>
                    </div>
            </div>
            <div class="col s12 m4 l4">
                    <div class="card medium">
                        <div class="card-image">
                                <img src="files/eventos/galerya/3.jpeg" alt="">
                        </div>
                        <div class="card-content">
                          <span class="card-title activator grey-text text-darken-4">Evento Curso Taller.</span>
                          <p><a>Curso 29-feb-2016</a></p>
                        </div>
                    </div>
            </div>
        </div>


    </div>
</div>

<!--Parallax-->
<div class="parallax-container">
    <div class="parallax"><img src="img/parallax1.png"></div>
</div>

<!--Team-->
<!--div class="section scrollspy" id="team">
    <div class="container">
        <h2 class="header text_b"> Our Team </h2>
        <div class="row">
            <div class="col s12 m3">
                <div class="card card-avatar">
                    <div class="waves-effect waves-block waves-light">
                        <img class="activator" src="img/avatar1.png">
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4">Flash <br/>
                            <small><em><a class="red-text text-darken-1" href="#">CEO</a></em></small></span>
                        <p>
                            <a class="blue-text text-lighten-2" href="https://www.facebook.com/joash.c.pereira">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                            <a class="blue-text text-lighten-2" href="https://twitter.com/im_joash">
                                <i class="fa fa-twitter-square"></i>
                            </a>
                            <a class="blue-text text-lighten-2" href="https://plus.google.com/u/0/+JoashPereira">
                                <i class="fa fa-google-plus-square"></i>
                            </a>
                            <a class="blue-text text-lighten-2" href="https://www.linkedin.com/in/joashp">
                                <i class="fa fa-linkedin-square"></i>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div-->

<!--Footer-->
<footer id="contact" class="page-footer default_color scrollspy">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <form class="col s12" action="contact.php" method="post">
                    <div class="row">
                        <div class="input-field col s6">
                            <i class="mdi-action-account-circle prefix white-text"></i>
                            <input id="icon_prefix" name="name" type="text" class="validate white-text">
                            <label for="icon_prefix" class="white-text">Nombre</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="mdi-communication-email prefix white-text"></i>
                            <input id="icon_email" name="email" type="email" class="validate white-text">
                            <label for="icon_email" class="white-text">Email/Correo</label>
                        </div>
                        <div class="input-field col s12">
                            <i class="mdi-editor-mode-edit prefix white-text"></i>
                            <textarea id="icon_prefix2" name="message" class="materialize-textarea white-text"></textarea>
                            <label for="icon_prefix2" class="white-text">Mensaje</label>
                        </div>
                        <div class="col offset-s7 s5">
                            <button class="btn waves-effect waves-light red darken-1" type="submit">Enviar
                                <i class="mdi-content-send right white-text"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <!--div class="col l3 s12">
                <h5 class="white-text"></h5>
                <ul>
                    <li><a class="white-text" href="#">Home</a></li>
                    <li><a class="white-text" href="#">Blog</a></li>
                </ul>
            </div-->
            <!--div class="col l3 s12">
                <h5 class="white-text">Social</h5>
                <ul>
                    <li>
                        <a class="white-text" href="#">
                            <i class="small fa fa-facebook-square white-text"></i> Facebook
                        </a>
                    </li>
                    <li>
                        <a class="white-text" href="#">
                            <i class="small fa fa-linkedin-square white-text"></i> Linkedin
                        </a>
                    </li>
                </ul>
            </div-->
        </div>
    </div>
    <div class="footer-copyright default_color">
      <div class="container">
        <div class="row">
          <div class="col l6 s12">
            diseño <a class="white-text">lic.salvador.ml@gmail.com / persocomsoft</a>
          </div>
          <div class="col l6 s12">
            <a href="#" class="white-text">© 2016-219 .Martinez de la torre. Todos los derechos reservados.</a>
          </div>
        </div>
      </div>
    </div>
</footer>

    <!--  Scripts-->
    <script src="js/jquery-2.1.1.min.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="min/plugin-min.js"></script>
    <script src="min/custom-min.js"></script>
    <script src="js/func.js"></script>

    </body>
</html>
