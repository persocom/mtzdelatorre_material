
function showEvent(q)
{
	$("#showEventosDiv").empty();
	var html = '<div class="col s12 m6">'
              +'<div class="card blue-grey darken-1">'
                +'<div class="card-content white-text">'
                  +'<span class="card-title">Archivos de descarga</span>'
                  +'<i class="mdi-navigation-close right" onclick="hideArchInfo(1);"></i>'
                  +'<p>Material disponible, descargar en el equipo que se usara para su correcto funcionamiento.'
                  +' Una vez abierto enviar impresión de pantalla con el codigo de asignación</p>'
                +'</div>'
                +'<div class="card-action">'
                  +'<a href="files/eventos/SOLUCION CP V7.0.1.zip"><i class="fa fa-cloud-download"></i> SolucionCP</a>'
                  +'<a href="files/eventos/Material Contab Electronica.rar"><i class="fa fa-cloud-download"></i>Material Contable</a><br>'
                  +'<a href="files/eventos/Analisis_practico_de_la_Contabilidad_Electronica_Mat.pdf" target="_blank"><i class="fa fa-cloud-download"></i>Documento PDF</a>'
                +'</div>'
                +'<!--div class="card-footer"><embed src="files/eventos/Análisis_practico_de_la_Contabilidad_Electrónica_Mat.pdf" width="500" height="375"></div-->'
              +'</div>'
            +'</div>';
	$("#showEventosDiv").append(html)
}

function hideArchInfo(q)
{

  $("#showEventosDiv").empty();
}
